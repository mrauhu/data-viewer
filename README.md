# Data Viewer

> TypeScript React application,
> based on https://github.com/react-boilerplate/react-boilerplate-cra-template

## Prerequisites

- Git;
- Node.js >= 12.

## Installation

```bash
git clone https://bitbucket.org/mrauhu/data-viewer
cd data-viewer
npm install
```

## Run

```bash
npm start
```
