import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100vh;
    width: 100%;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #root {
    min-height: 100vh;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }

  input, select {
    font-family: inherit;
    font-size: inherit;
  }
  
  main {
    display: grid;
    grid-template-rows: calc(100vh - 25px - 64px) 25px;
    grid-template-columns: 1fr 2fr 3fr;
    grid-template-areas:
      "suptype type doc"
      ". . date";
    justify-content: inherit;
    height: inherit;
  }
  main > nav {
    grid-area: suptype;
  }
  main > ul {
    grid-area: type;
  }
  main > article {
    grid-area: doc;
    padding: 2em;
  }
  main > footer {
    grid-area: date;
    justify-self: right;
    align-self: end;
  }
  
  .cloud-icon {
    margin-left: 0.5em;
    margin-bottom: 0.5em;
  }
`;
