import { NavLink, Route } from 'react-router-dom';
import { Item } from './Item';
import React from 'react';
import { flatTypes } from './types';
import { Helmet } from 'react-helmet-async';
import { List, ListItem } from '@material-ui/core';

export const Type = ({ match }) => {
  const {
    params: { typeName },
  } = match;
  const type = flatTypes.find(({ name }) => name === typeName) ?? {
    name: '',
    list: [],
  };

  return (
    <>
      <Helmet>
        <title>{typeName}</title>
      </Helmet>
      <List>
        {type.list.map((item, index) => {
          return (
            <ListItem
              key={index}
              component={NavLink}
              button
              activeClassName="Mui-selected"
              to={`${match.url}/item/${item.name}`}
            >
              {item.name}
            </ListItem>
          );
        })}
      </List>

      <Route path={`${match.path}/item/:itemName`} component={Item} />
    </>
  );
};
