import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { Header } from './Header';
import { Sidebar } from './Sidebar';
import { Type } from './Type';
import { Footer } from './Footer';

export function HomePage() {
  return (
    <Router>
      <Header />
      <main>
        <Sidebar />
        <Route path="/type/:typeName" component={Type} />
        <Footer />
      </main>
    </Router>
  );
}
