import React from 'react';
import { AppBar, Button, Toolbar, Typography } from '@material-ui/core';
import { AccountCircle } from '@material-ui/icons';
import makeStyles from '@material-ui/core/styles/makeStyles';

export function Header() {
  const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    title: {
      flexGrow: 1,
    },
    userName: {
      marginRight: theme.spacing(1),
    },
  }));

  const classes = useStyles();

  return (
    <>
      <AppBar position="static" className={classes.root}>
        <Toolbar>
          <Typography className={classes.title} variant="h6">
            Тестовое задание
          </Typography>
          <div>
            <Button
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              color="inherit"
            >
              <span className={classes.userName}>Sergey N</span>
              <AccountCircle />
            </Button>
          </div>
        </Toolbar>
      </AppBar>
    </>
  );
}
