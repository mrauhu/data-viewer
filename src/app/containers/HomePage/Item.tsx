import React from 'react';
import { flatTypes } from './types';
import { Helmet } from 'react-helmet-async';
import { Paper, Typography } from '@material-ui/core';
import CloudDoneIcon from '@material-ui/icons/CloudDone';

type TabPageProps = {
  match: {
    params: {
      typeName: string;
      itemName: string;
    };
  };
};
export const Item = ({ match }: TabPageProps) => {
  const {
    params: { typeName, itemName },
  } = match;

  const type = flatTypes.find(({ name }) => name === typeName) ?? { list: [] };
  const item = type.list.find(({ name }) => name === itemName) ?? {
    name: '',
    content: [],
  };

  return (
    <>
      <Helmet>
        <title>
          {item.name} — {typeName}
        </title>
      </Helmet>
      <Paper component="article">
        <Typography variant="h3">
          {item.name}
          <span title="Сохранено">
            <CloudDoneIcon className="cloud-icon" />
          </span>
        </Typography>
        <Typography>{item.content}</Typography>
      </Paper>
    </>
  );
};
