import React from 'react';
import { NavLink } from 'react-router-dom';
import { List, ListItem, ListSubheader } from '@material-ui/core';
import { types } from './types';

export function Sidebar() {
  return (
    <List component="nav">
      {types.map((supType, key) => {
        return (
          <>
            <ListSubheader key={key}>{supType.name}</ListSubheader>
            {supType.list.map((type, index) => {
              return (
                <ListItem
                  key={index}
                  component={NavLink}
                  button
                  activeClassName="Mui-selected"
                  to={`/type/${type.name}`}
                >
                  {type.name}
                </ListItem>
              );
            })}
          </>
        );
      })}
    </List>
  );
}
