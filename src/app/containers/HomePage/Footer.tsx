import React, { useState, useEffect } from 'react';

const EVERY_MINUTE = 60 * 1000;

export function Footer() {
  const [dateTime, setDateTime] = useState(new Date());

  useEffect(() => {
    const id = setInterval(() => setDateTime(new Date()), EVERY_MINUTE);
    return () => {
      clearInterval(id);
    };
  }, []);

  return <footer>{dateTime.toLocaleDateString()}</footer>;
}
