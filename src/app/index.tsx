/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { BrowserRouter } from 'react-router-dom';

import { GlobalStyle } from 'styles/global-styles';

import { HomePage } from './containers/HomePage/Loadable';

export function App() {
  return (
    <BrowserRouter>
      <Helmet
        titleTemplate="%s - Тестовое задание"
        defaultTitle="Тестовое задание"
      >
        <meta
          name="description"
          content="Пример проект на React & TypeScript"
        />
      </Helmet>

      <HomePage />
      <GlobalStyle />
    </BrowserRouter>
  );
}
